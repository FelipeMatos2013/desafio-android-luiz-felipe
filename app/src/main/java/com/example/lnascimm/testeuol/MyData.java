package com.example.lnascimm.testeuol;

public class MyData {

    private int id;
    private String title;
    private String description;
    private String authorName;
    private String image_teaser_link;
    private String image_normal_link;



    public MyData(int id, String title, String description, String authorName, String image_teaser_link, String image_normal_link) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.authorName = authorName;
        this.image_teaser_link = image_teaser_link;
        this.image_normal_link = image_normal_link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getImage_teaser_link() {
        return image_teaser_link;
    }

    public void setImage_teaser_link(String image_teaser_link) {
        this.image_teaser_link = image_teaser_link;
    }

    public String getImage_normal_link() {
        return image_normal_link;
    }

    public void setImage_normal_link(String image_normal_link) {
        this.image_normal_link = image_normal_link;
    }
}
package com.example.lnascimm.testeuol;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import static android.R.attr.author;

public class ShotDetail extends AppCompatActivity {

    ImageView image;
    TextView author, description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_detail);


        image = (ImageView) findViewById(R.id.imageDetail);
        author = (TextView) findViewById(R.id.authorName);
        description = (TextView) findViewById(R.id.descriptionDetail);

        Glide.with(this)
                .load(getIntent().getStringExtra("imageDetail"))
                .into(image);

       author.setText(getIntent().getStringExtra("authorName"));
       description.setText(Html.fromHtml(getIntent().getStringExtra("descriptionDetail")));
    }


}

package com.example.lnascimm.testeuol;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lnascimm.testeuol.MyData;
import com.example.lnascimm.testeuol.R;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.title;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

    private Context context;
    private List<MyData> my_data;

    public CustomAdapter(Context context, List<MyData> my_data) {
        this.context = context;
        this.my_data = my_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card,parent,false);
        ViewHolder viewHolder = new ViewHolder(itemView, context,my_data);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.title.setText(my_data.get(position).getTitle());
        Glide.with(context).load(my_data.get(position).getImage_teaser_link()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView title;
        public ImageView imageView;
        List <MyData> my_data = new ArrayList<MyData>();
        Context context;
        public ViewHolder(View itemView, Context context, List<MyData> my_data) {
            super(itemView);
            this.my_data = my_data;
            this.context = context;
            itemView.setOnClickListener(this);
            title = (TextView) itemView.findViewById(R.id.title);
            imageView = (ImageView) itemView.findViewById(R.id.image);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            MyData my_data = this.my_data.get(position);
            Intent intent = new Intent(this.context, ShotDetail.class);
            intent.putExtra("authorName", my_data.getAuthorName());
            intent.putExtra("descriptionDetail", my_data.getDescription());
            intent.putExtra("imageDetail", my_data.getImage_normal_link());
            this.context.startActivity(intent);
        }
    }
}